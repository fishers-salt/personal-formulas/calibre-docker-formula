# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as calibre_docker with context %}

{%- set mountpoint = calibre_docker.docker.mountpoint %}

{#
calibre-docker-mountdir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/calibre
    - makedirs: True
#}

calibre-docker-image-present:
  docker_image.present:
    - name: {{ calibre_docker.container.image }}
    - tag: {{ calibre_docker.container.tag }}
    - force: True

calibre-docker-container-running:
  docker_container.running:
    - name: {{ calibre_docker.container.name }}
    - image: {{ calibre_docker.container.image }}:{{ calibre_docker.container.tag }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/shares/books:/config
    - port_bindings:
      - 8480:8080
      - 8481:8081
      - 8581:8181
    - dns: {{ calibre_docker.dns_servers }}
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
      - CUSTOM_USER: {{ calibre_docker.owner.user }}
      - PASSWORD: {{ calibre_docker.password }}
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.calibre-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.calibre-web.middlewares=calibre-redirect-websecure"
      - "traefik.http.routers.calibre-web.rule=Host(`{{ calibre_docker.container.url }}`)"
      - "traefik.http.routers.calibre-web.entrypoints=web"
      - "traefik.http.routers.calibre-websecure.rule=Host(`{{ calibre_docker.container.url }}`)"
      - "traefik.http.routers.calibre-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.calibre-websecure.tls=true"
      - "traefik.http.routers.calibre-websecure.entrypoints=websecure"
      - "traefik.http.services.calibre.loadBalancer.server.port=8480"
    - require:
      - calibre-docker-image-present
