# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as calibre_docker with context %}

calibre-docker-container-absent:
  docker_container.absent:
    - name: {{ calibre_docker.container.name }}
    - force: True

calibre-docker-image-absent:
  docker_image.absent:
    - name: {{ calibre_docker.container.image }}
