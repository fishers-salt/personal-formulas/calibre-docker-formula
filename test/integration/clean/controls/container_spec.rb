# frozen_string_literal: true

describe docker.containers do
  its('names') { should_not include 'calibre' }
  its('images') { should_not include 'lscr.io/linuxserver/calibre:latest' }
end
