# frozen_string_literal: true

describe docker_container(name: 'calibre') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'lscr.io/linuxserver/calibre:latest' }
  it { should have_volume('/config', '/srv/shares/books') }
end
